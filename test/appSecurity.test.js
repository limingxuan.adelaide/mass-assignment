const request = require('supertest');
const app = require('./app');


describe('security', () => {

    it('Request to store user role, should return 400', async () => {
        const res = await request(app)
            .post('/')
            .send('{"name": "Foo Bar", "email": "foo@bar.com", "role": "admin"}')
            .set('Content-Type', 'application/json');
        expect(res.statusCode).toEqual(400);
    });

    it('Request to store user id, should return 400', async () => {
        const res = await request(app)
            .post('/')
            .send('{"name": "Foo Bar", "email": "foo@bar.com", "id": 1}')
            .set('Content-Type', 'application/json');
        expect(res.statusCode).toEqual(400);
    });

    it('Request to add new User property, should return 400', async () => {
        const res = await request(app)
            .post('/')
            .send('{"name": "Foo Bar", "email": "foo@bar.com", "foo": "bar"}')
            .set('Content-Type', 'application/json');
        expect(res.statusCode).toEqual(400);
    });

    it('Request to add empty email field, should return 400', async () => {
        const res = await request(app)
            .post('/')
            .send('{"name": "Foo Bar", "email": ""}')
            .set('Content-Type', 'application/json');
        expect(res.statusCode).toEqual(400);
    });

    it('Request to add invalid email field, should return 400', async () => {
        const res = await request(app)
            .post('/')
            .send('{"name": "Foo Bar", "email": "foobar.com"}')
            .set('Content-Type', 'application/json');
        expect(res.statusCode).toEqual(400);
    });

    it('Request to add long email field, should return 400', async () => {
        const res = await request(app)
            .post('/')
            .send('{"name": "Foo Bar", "email": "ssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss@sssssssssssssssssssssssssss.com"}')
            .set('Content-Type', 'application/json');
        expect(res.statusCode).toEqual(400);
    });

    it('Request to add empty user field should return 400', async () => {
        const res = await request(app)
            .post('/')
            .send('{"name": "", "email": "foobar.com"}')
            .set('Content-Type', 'application/json');
        expect(res.statusCode).toEqual(400);
    });

    it('Request to add long user field should return 400', async () => {
        const res = await request(app)
            .post('/')
            .send('{"name": "sssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss", "email": "foobar.com"}')
            .set('Content-Type', 'application/json');
        expect(res.statusCode).toEqual(400);
    });

    it('Request to add valid User property, should return 200', async () => {
        const res = await request(app)
            .post('/')
            .send('{"name": "Foo Bar", "email": "foo@bar.com"}')
            .set('Content-Type', 'application/json');
        expect(res.statusCode).toEqual(200);
    });
});
