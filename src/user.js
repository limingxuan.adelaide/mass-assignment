"use strict";

// User data model
class User {
    constructor(name, email) {
        if (typeof email !== 'string' ||
            email.length === 0 ||
            email.length > 100 ||
            !validateEmail(email)
        ) {
            throw new TypeError("Invalid email");
        }

        if (typeof name !== 'string' ||
            name.length === 0 ||
            name.length > 100
        ) {
            throw new TypeError("Invalid name");
        }

        this.email = email;
        this.name = name;
        this.id = auth_user_id();
        this.role = "user";

        Object.seal(this);
    }
}

const validateEmail = (email) => {
    const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}

// A psudo authenticator
// returns authenticated user ID
function auth_user_id() {
    return 2;
}


module.exports = { User, auth_user_id };
